<?php

namespace App\Controller;
use App\Entity\Pelicula;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PeliculasController extends AbstractController {
    /**
     * @Route("/peliculas")
     */
    public function listadoPeliculas(){
        $peliculas = [
            new Pelicula('El Padrino', 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/dfEQMuZMIcPgC7nt07D9uVQi7Tv.jpg','Don Vito Corleone, conocido dentro de los círculos del hampa como "El Padrino", es el patriarca de una de las cinco familias que ejercen el mando de la Cosa Nostra en Nueva York en los años 40. Don Corleone tiene cuatro hijos; una chica, Connie, y tres varones, Sonny, Michael y Freddie, al que envían exiliado a Las Vegas, dada su incapacidad para asumir puestos de mando en la "Familia". Cuando el Padrino reclina intervenir en el negocio de estupefacientes, empieza una cruenta lucha de violentos episodios entre las distintas familias del crimen organizado.'),
            new Pelicula('El padrino II','https://image.tmdb.org/t/p/w600_and_h900_bestv2/vNR6SOKbOkj94gNfc2sJkQjeAe1.jpg','Continuación de la saga de los Corleone con dos historias paralelas: la elección de Michael Corleone como jefe de los negocios familiares y los orígenes del patriarca, el ya fallecido Don Vito, primero en Sicilia y luego en Estados Unidos, donde, empezando desde abajo, llegó a ser un poderosísimo jefe de la mafia de Nueva York.'),
            new Pelicula('Uno de los Nuestros','https://image.tmdb.org/t/p/w600_and_h900_bestv2/jpa7yVzgPSSK3edOhA6gnngS9RR.jpg','Henry, un niño de trece años de Brooklyn, vive fascinado con el mundo de los gángsters. Su sueño se hace realidad cuando entra a formar parte de la familia Pauline, dueña absoluta de la zona, que lo educan como un miembro más de la banda convirtiéndole en un destacado mafioso.')
        ];
        return $this->render('peliculas.html.twig',
            [
                'pelis' => $peliculas,
                'fecha' => new \DateTime("now")
            ]
        );
    }
}
