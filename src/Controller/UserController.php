<?php

namespace App\Controller;
use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController {
    /**
     * @Route("/users")
     */
    public function listadoUsuarios(){
        $usuarios = [
            new User('Pepe', true),
            new User('Mongo Mongo',true),
            new User('Pepeee', false),
            new User('Anibal', true)
        ];
        return $this->render('user.html.twig',
            [
                'users' => $usuarios,
                'fecha' => new \DateTime("now")
            ]
        );
    }
}
