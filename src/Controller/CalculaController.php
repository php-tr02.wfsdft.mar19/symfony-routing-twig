<?php 

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculaController {

    /**
     * @Route("/calcula/{parametro}/", requirements={"parametro": "\d+"})
     */

    public function calculaNum($parametro){
        if (is_numeric($parametro)){
            if ($parametro % 2)
            {
                return new Response("Es Numero IM-PAR $parametro");
            }
            else return new Response("Es Numero PAR $parametro");   
        } 
    }

    /**
     * @Route("/calcula/{parametro}/", requirements={"parametro": "\w+"})
     */

    public function calculaPal($parametro){
        if (is_string($parametro)){
            $palabro = '';
            for ($i=0;$i<strlen($parametro); $i++) {
                if ($i % 2) {
                    $palabro = $palabro . $parametro{$i};
                }   
                else {
                    $palabro = $palabro . strtoupper($parametro{$i});
                }
            }
            return new Response("Es palabra $palabro");
        }
    }  
}