<?php

namespace App\Controller;


use App\Entity\PuntuacionUsuarios;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PuntuacionController extends AbstractController {

    /**
     * @Route("/puntuacion")
     */
    public function listadoPuntuacion(){
        $usuariosPuntuacion = [
            new PuntuacionUsuarios('Moi',7.8,'Extremadura'),
            new PuntuacionUsuarios('Kiko',8,'Oviedo'),
            new PuntuacionUsuarios('Jorge',2.53,'De la vida'),
            new PuntuacionUsuarios('',3.5,'Hogwarts'),
            new PuntuacionUsuarios('Ana',7.25,'Oviedo')
        ];

        usort($usuariosPuntuacion, function($a, $b)
        {
            return strcmp($a->getPuntuacion(), $b->getPuntuacion());
        });

        return $this->render('puntuacion.html.twig',
            [
                'users' => $usuariosPuntuacion,
                'fecha' => new \DateTime("now")
            ]
        );
    }

    /*public function cmp ($a,$b){
        if ($a->puntuacion == $b->puntuacion){
            return 0;
        }
        return ($a->puntuacion < $b->puntuacion)? -1 : 1;
    }*/
}
