<?php 

namespace App\Controller;
use App\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Twig;

class DinamicController extends AbstractController {

    /**
     * @Route("/dinamic/")
     */

    public function user(){

        $usuarios = [
            new User('Mongo'),
            new User('Pepe'),
            new User('Paco')
        ];
        //return new Response("Usuario $id");
        return $this->render('user.html.twig',
        ['usuario' => $usuarios]
    );
    }
}
