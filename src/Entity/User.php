<?php

namespace App\Entity;


class User
{
    private $nombrePersona;
    private $registrado;

    public function __construct(string $nombrePersona , bool $registrado)
    {
        $this->nombrePersona = $nombrePersona;
        $this->registrado = $registrado;
    }

    /**
     * @return string
     */
    public function getNombrePersona(): string
    {
        return $this->nombrePersona;
    }

    /**
     * @return bool
     */
    public function isRegistrado(): bool
    {
        return $this->registrado;
    }


}
