<?php

namespace App\Entity;


class PuntuacionUsuarios {
    private $nombre;
    private $puntuacion;
    private $universidad;

    /**
     * PuntuacionUsuarios constructor.
     * @param $nombre
     * @param $puntuacion
     * @param $universidad
     */
    public function __construct(String $nombre = "", float $puntuacion, String $universidad)
    {
        $this->nombre = $nombre;
        $this->puntuacion = $puntuacion;
        $this->universidad = $universidad;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getPuntuacion()
    {
        return $this->puntuacion;
    }

    /**
     * @return mixed
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }




}
