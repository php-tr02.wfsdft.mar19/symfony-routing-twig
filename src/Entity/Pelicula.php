<?php

namespace App\Entity;


class Pelicula
{
    private $nombrePelicula;
    private $urlImagen;
    private $sinopsis;

    public function __construct(string $nombrePelicula , string $urlImagen, string $sinopsis)
    {
        $this->nombrePelicula = $nombrePelicula;
        $this->urlImagen = $urlImagen;
        $this->sinopsis = $sinopsis;
    }

    /**
     * @return mixed
     */
    public function getSinopsis()
    {
        return $this->sinopsis;
    }

    /**
     * @return string
     */
    public function getNombrePelicula(): string
    {
        return $this->nombrePelicula;
    }

    /**
     * @return string
     */
    public function getUrlImagen(): string
    {
        return $this->urlImagen;
    }




}
